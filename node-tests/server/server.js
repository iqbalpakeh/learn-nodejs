const utils = require("../utils/utils");
const express = require("express");
var app = express();

app.get("/", (req, res) => {
  res.send("Hello world!");
});

app.get("/about", (req, res) => {
  res.status(404).send({
    error: "Page not found.",
    name: "Todo App v1.0"
  });
});

app.get("/users", (req, res) => {
  res.send([
    { name: "Iqbal", age: 34 },
    { name: "Hanifah", age: 34 },
    { name: "Safiya", age: 5 }
  ]);
});

app.listen("3000", () => {
  utils.log("Listen @ port 3000");
});

module.exports.app = app;
