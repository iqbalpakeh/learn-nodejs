const request = require("supertest");
const expect = require("expect");

var app = require("./server").app;

describe("Server", () => {
  describe("Get /", () => {
    it("should return hello world resonse", done => {
      request(app)
        .get("/")
        .expect(200)
        .expect("Hello world!")
        .end(done);
    });
  });
  describe("Get /about", () => {
    it("should return 404 and Page not found", done => {
      request(app)
        .get("/about")
        .expect(404)
        .expect(res => {
          expect(res.body).toInclude({
            error: "Page not found."
          });
        })
        .end(done);
    });
  });
  describe("Get /users", () => {
    it("should return my user object", done => {
      request(app)
        .get("/users")
        .expect(200)
        .expect(res => {
          expect(res.body).toInclude({
            name: "Iqbal",
            age: 34
          });
        })
        .end(done);
    });
  });
});
