const expect = require("expect");
const rewire = require("rewire");

var app = rewire("./app");

describe("app", () => {
  var db = {
    saveUser: expect.createSpy()
  };
  app.__set__("db", db);

  it("should call the spy correctly", () => {
    var spy = expect.createSpy();
    spy("Iqbal", 34);
    expect(spy).toHaveBeenCalled("Iqbal", 34);
  });

  it("should call saveUser with user object", () => {
    var email = "iqbalpakeh@gmail.com";
    var password = "123abc";

    app.handleSignup(email, password);
    expect(db.saveUser).toHaveBeenCalledWith({ email, password });
  });
});
