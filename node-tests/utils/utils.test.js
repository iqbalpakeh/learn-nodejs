const expect = require("expect");
const utils = require("./utils");

describe("Utils", () => {
  describe("#add", () => {
    it("should add two numbers", () => {
      var res = utils.add(33, 11);
      expect(res)
        .toBe(44)
        .toBeA("number");
    });

    it("should async add two numbers", done => {
      utils.asyncAdd(3, 4, sum => {
        expect(sum)
          .toBe(7)
          .toBeA("number");
        done();
      });
    });
  });

  describe("#square", () => {
    it("should square a number", () => {
      var res = utils.square(10);
      expect(res)
        .toBe(100)
        .toBeA("number");
    });

    it("should async square a number", done => {
      utils.asyncSquare(3, square => {
        expect(square)
          .toBe(9)
          .toBeA("number");
        done();
      });
    });
  });

  describe("#object", () => {
    it("should expect some values", () => {
      expect(12).toNotBe(11);
      expect({ name: "iqbal" }).toEqual({ name: "iqbal" });
      expect([2, 3, 4]).toExclude(1);
    });

    it("should verify first and last name are set", () => {
      var user = { age: 25, location: "Singapore" };
      var res = utils.setName(user, "Iqbal Pakeh");

      expect(user).toEqual(res);

      expect(user.firstName)
        .toBeA("string")
        .toBe("Iqbal");

      expect(user.lastName)
        .toBeA("string")
        .toBe("Pakeh");

      expect(res).toInclude({
        firstName: "Iqbal",
        lastName: "Pakeh"
      });
    });
  });
});
