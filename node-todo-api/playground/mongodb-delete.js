/**
 * Connect to mongodb server
 */
const { MongoClient, ObjectID } = require("mongodb");
MongoClient.connect("mongodb://localhost:27017/TodoApp", (err, db) => {
  if (err) {
    return console.log("Unable to connect to mongodb server");
  }
  console.log("Connected to mongodb server");

  // Delete many
  if (false) {
    db.collection("Todos")
      .deleteMany({ text: "Eat lunch" })
      .then(result => {
        console.log(result);
      });
  }

  // Delete one
  if (false) {
    db.collection("Todos")
      .deleteOne({ text: "Eat lunch" })
      .then(result => {
        console.log(result);
      });
  }

  // Find one and delete
  if (false) {
    db.collection("Todos")
      .findOneAndDelete({ completed: true })
      .then(result => {
        console.log(result);
      });
  }

  // Challenge #1
  if (false) {
    db.collection("Users")
      .deleteMany({ name: "Mochammed Iqbal Pakeh" })
      .then(result => {
        console.log(result);
      });
  }

  // Challenge #2
  if (true) {
    db.collection("Users")
      .findOneAndDelete({ _id: ObjectID("5c71f66a1ec259653d0e3c8c") })
      .then(result => {
        console.log(result);
      });
  }

  // db.close();
});
