/**
 * Connect to mongodb server
 */
const { MongoClient, ObjectID } = require("mongodb");
MongoClient.connect("mongodb://localhost:27017/TodoApp", (err, db) => {
  if (err) {
    return console.log("Unable to connect to mongodb server");
  }
  console.log("Connected to mongodb server");

  // Query all tasks
  if (false) {
    db.collection("Todos")
      .find()
      .toArray()
      .then(
        docs => {
          console.log("Todos");
          console.log(JSON.stringify(docs, undefined, 2));
        },
        err => {
          console.log("Unable to fetch todos", err);
        }
      );
  }

  // Query only the completed tasks
  if (false) {
    db.collection("Todos")
      .find({ completed: false })
      .toArray()
      .then(
        docs => {
          console.log("Todos");
          console.log(JSON.stringify(docs, undefined, 2));
        },
        err => {
          console.log("Unable to fetch todos", err);
        }
      );
  }

  // Query with specific id
  if (false) {
    db.collection("Todos")
      .find({ _id: new ObjectID("5c71dd236383405834bd892b") })
      .toArray()
      .then(
        docs => {
          console.log("Todos");
          console.log(JSON.stringify(docs, undefined, 2));
        },
        err => {
          console.log("Unable to fetch todos", err);
        }
      );
  }

  // Query using count instead of array
  if (false) {
    db.collection("Todos")
      .find()
      .count()
      .then(
        count => {
          console.log(`Todos count: ${count}`);
        },
        err => {
          console.log("Unable to fetch todos", err);
        }
      );
  }

  // Query Users db with specific name
  if (true) {
    db.collection("Users")
      .find({ name: "Mochammed Iqbal Pakeh" })
      .toArray()
      .then(
        docs => {
          console.log("Users");
          console.log(JSON.stringify(docs, undefined, 2));
        },
        err => {
          console.log("Unable to fetch todos", err);
        }
      );
  }

  // db.close();
});
