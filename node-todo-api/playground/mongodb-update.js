/**
 * Connect to mongodb server
 */
const { MongoClient, ObjectID } = require("mongodb");
MongoClient.connect("mongodb://localhost:27017/TodoApp", (err, db) => {
  if (err) {
    return console.log("Unable to connect to mongodb server");
  }
  console.log("Connected to mongodb server");

  // Find one and update
  if (false) {
    db.collection("Todos")
      .findOneAndUpdate(
        {
          _id: new ObjectID("5c71f7954013933d99715d93")
        },
        {
          $set: {
            completed: true
          }
        },
        {
          returnedOriginal: false
        }
      )
      .then(result => {
        console.log(result);
      });
  }

  // Challenge #1
  if (true) {
    db.collection("Users")
      .findOneAndUpdate(
        {
          _id: new ObjectID("5c7220dd4013933d9971611a")
        },
        {
          $set: {
            name: "Aziza Ayunindya"
          },
          $inc: {
            age: 1
          }
        },
        {
          returnedOriginal: false
        }
      )
      .then(result => {
        console.log(result);
      });
  }

  // db.close();
});
