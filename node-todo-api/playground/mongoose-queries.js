const { ObjectID } = require("mongodb");

const { mongoose } = require("./../server/db/mongoose");
const { Todo } = require("./../server/models/todo");
const { User } = require("./../server/models/user");

/**
 * Define id by looking at Robomongo
 */
var id = "5c7492f1bd3e6858180a29b4";
var userId = "5c7310be5bc8bd7d763a34a4";

/**
 * Check if id is valid
 */
if (!ObjectID.isValid(id)) {
  return console.log("id is not valid");
}

/**
 * Finding many item with the same query info
 */
Todo.find({
  _id: id
}).then(todos => {
  console.log("todos", todos);
});

/**
 * Find one item with one query info
 */
Todo.findOne({
  _id: id
}).then(todo => {
  console.log("todo", todo);
});

/**
 * Find one item with only by id
 */
Todo.findById(id)
  .then(todo => {
    if (!todo) {
      return console.log("id not found");
    }
    console.log("todo by id", todo);
  })
  .catch(e => console.log(e));

/**
 * Find one user by id
 */
User.findById(userId)
  .then(user => {
    if (!user) {
      return console.log("id not found");
    }
    console.log(JSON.stringify(user, undefined, 2));
  })
  .catch(e => console.log(e));
