/**
 * import mongoose library
 */
var mongoose = require("mongoose");

/**
 * setting mongoose to use promise
 * instead of callback
 */
mongoose.Promise = global.Promise;

/**
 * connect to mongodb server running
 * on localhost port 27017
 */
mongoose.connect("mongodb://localhost:27017/TodoApp");

/**
 * Export modules
 */
module.exports = { mongoose };
