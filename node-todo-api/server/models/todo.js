var mongoose = require("mongoose");

/**
 * define Todo model with monggose
 */
var Todo = mongoose.model("Todo", {
  text: {
    type: String,
    required: true,
    minlength: 1,
    trim: true
  },
  completed: {
    type: Boolean,
    default: false
  },
  completedAt: {
    type: Number,
    default: null
  }
});

/**
 * Export modules
 */
module.exports = { Todo };
