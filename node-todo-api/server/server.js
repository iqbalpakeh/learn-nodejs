/**
 * Import node module
 */
var express = require("express");
var bodyParser = require("body-parser");
var { ObjectID } = require("mongodb");

/**
 * Import local module
 */
var { mongoose } = require("./db/mongoose");
var { Todo } = require("./models/todo");
var { User } = require("./models/user");

/**
 * Create express application
 */
var app = express();

/**
 * Use middleware to help on parsing http body
 */
app.use(bodyParser.json());

/**
 * Handle todo creation endpoint: "/todos"
 */
app.post("/todos", (req, res) => {
  var todo = new Todo({
    text: req.body.text
  });
  todo.save().then(
    doc => {
      res.send(doc);
    },
    e => {
      res.status(400).send(e);
    }
  );
});

/**
 * Handle get list of todo endpoint: "/todos"
 */
app.get("/todos", (req, res) => {
  Todo.find().then(
    todos => {
      res.send({ todos });
    },
    e => {
      res.status(400).send(e);
    }
  );
});

/**
 * Handle get todo with id endpoint : "/todos/:id"
 */
app.get("/todos/:id", (req, res) => {
  var id = req.params.id;

  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }

  Todo.findById(id)
    .then(todo => {
      if (!todo) {
        return res.status(404).send();
      }
      res.send({ todo });
    })
    .catch(() => res.status(404).send());
});

/**
 * Start server and listen on port 3000
 */
app.listen(3000, () => {
  console.log("Started on port 3000");
});

/**
 * Export module
 */
module.exports = { app };
