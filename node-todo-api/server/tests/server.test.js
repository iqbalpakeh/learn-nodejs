/**
 * Import node module
 */
const expect = require("expect");
const request = require("supertest");
const { ObjectID } = require("mongodb");

/**
 * Import local module
 */
const { app } = require("../server");
const { Todo } = require("../models/todo");

/**
 * List of dummy todo for testing
 */
const todos = [
  {
    _id: new ObjectID("5c7492f1bd3e6858180a29b4"),
    text: "First things todo"
  },
  {
    _id: new ObjectID("5c7492f1bd3e6858180a29b5"),
    text: "Second things todo"
  }
];

/**
 * Clean data base before executing test
 */
beforeEach(done => {
  Todo.remove({})
    .then(() => {
      return Todo.insertMany(todos);
    })
    .then(() => done())
    .catch(e => console.log(e));
});

/**
 * Define test for POST "/todos" api
 */
describe("POST /todos", () => {
  it("should create a new todo", done => {
    var text = "Test todo text";

    request(app)
      .post("/todos")
      .send({ text })
      .expect(200)
      .expect(res => {
        expect(res.body.text).toBe(text);
      })
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        Todo.find({ text })
          .then(todos => {
            expect(todos.length).toBe(1);
            expect(todos[0].text).toBe(text);
            done();
          })
          .catch(e => done(e));
      });
  });

  it("should not create todo with invalid body data", done => {
    request(app)
      .post("/todos")
      .send({})
      .expect(400)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        Todo.find()
          .then(todos => {
            expect(todos.length).toBe(2);
            done();
          })
          .catch(e => done(e));
      });
  });
});

/**
 * Define test for GET "/todos" api
 */
describe("GET /todos", () => {
  it("should get all todos", done => {
    request(app)
      .get("/todos")
      .expect(200)
      .expect(res => {
        expect(res.body.todos.length).toBe(2);
      })
      .end(done);
  });
});

/**
 * Define test for GET "/todos/:id" api
 */
describe("GET /todos:id", () => {
  it("should return todo doc", done => {
    request(app)
      .get(`/todos/${todos[0]._id.toHexString()}`)
      .expect(200)
      .expect(res => {
        expect(res.body.todo.text).toBe(todos[0].text);
      })
      .end(done);
  });

  it("should return 404 for non-object ids", done => {
    request(app)
      .get("/todos/123")
      .expect(404)
      .end(done);
  });
});
