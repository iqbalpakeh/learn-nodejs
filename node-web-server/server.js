/**
 * How to run:
 * $ nodemon server.js -e js,hbs
 *
 * note:
 * -e is to watch other files other than *.js
 */

// import all necessary module
const express = require("express");
const hbs = require("hbs");
const fs = require("fs");

/**
 * Define hbs to use partials
 */
hbs.registerPartials(__dirname + "/views/partials");

/**
 * define express app
 */
var app = express();

/**
 * define hbs (handlebars) as express view engine
 */
app.set("view engine", "hbs");

/**
 * Define express middleware to log all the server
 * request
 */
app.use((req, res, next) => {
  var now = new Date().toString();
  var log = `${now}: ${req.method} ${req.url}`;
  console.log(log);
  fs.appendFile("server.log", log + "\n", err => {
    if (err) {
      console.log("Unable to append the server log.");
    }
  });
  next();
});

/**
 * Define express middleware to show the site is
 * currently undermaintenance [ONLY USE IF NEEDED]
 */
// app.use((req, res, next) => {
//   res.render("maintenance.hbs");
// });

/**
 * Define express middleware to serve static data
 * on folder "/public"
 */
app.use(express.static(__dirname + "/public"));

/**
 * Register getCurrentYear to handlebars
 */
hbs.registerHelper("getCurrentYear", () => {
  return new Date().getFullYear();
});

/**
 * Register screamIt to handlebars
 */
hbs.registerHelper("screamIt", text => {
  return text.toUpperCase();
});

/**
 * Route for "/"
 */
app.get("/", (req, res) => {
  res.render("home.hbs", {
    pageTitle: "Home Page",
    welcomeMessage: "Welcome to my website",
    currentYear: new Date().getFullYear()
  });
});

/**
 * Route for "/about"
 */
app.get("/about", (req, res) => {
  res.render("about.hbs", {
    pageTitle: "About Page"
  });
});

/**
 * Route for "/bad"
 */
app.get("/bad", (req, res) => {
  res.send({
    errorMessage: "Unable to handle request"
  });
});

/**
 * Start web server
 */
app.listen(3000, () => {
  console.log("Server is up at port 3000");
});
