const fs = require("fs");

/**
 * Fetch notes from json file
 */
var fetchNotes = () => {
  try {
    var notesString = fs.readFileSync("notes-data.json");
    return JSON.parse(notesString);
  } catch (e) {
    return [];
  }
};

/**
 * Save notes to json file
 *
 * @param {*} notes
 */
var saveNotes = notes => {
  fs.writeFileSync("notes-data.json", JSON.stringify(notes));
};

/**
 * Add new note to json file
 *
 * @param {*} title
 * @param {*} body
 */
var addNote = (title, body) => {
  var notes = fetchNotes();
  var note = {
    title,
    body
  };

  var duplicateNotes = notes.filter(note => note.title === title);
  if (duplicateNotes.length === 0) {
    notes.push(note);
    saveNotes(notes);
    return note;
  }
};

/**
 * Retrieve all notes from json file
 */
var getAll = () => {
  return fetchNotes();
};

/**
 * Get a note by title from json file
 *
 * @param {*} title
 */
var getNote = title => {
  var notes = fetchNotes();
  var filteredNotes = notes.filter(note => note.title === title);
  return filteredNotes[0];
};

/**
 * Remove a note by title from json file
 *
 * @param {*} title
 */
var removeNote = title => {
  var notes = fetchNotes();
  var filteredNotes = notes.filter(note => note.title !== title);
  saveNotes(filteredNotes);

  return notes.length !== filteredNotes.length;
};

/**
 * Log the note content
 */
var logNote = note => {
  console.log("--");
  console.log(`Title: ${note.title}`);
  console.log(`Body: ${note.body}`);
};

module.exports = {
  addNote,
  getAll,
  getNote,
  removeNote,
  logNote
};
