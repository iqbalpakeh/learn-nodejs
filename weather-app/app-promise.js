const yargs = require("yargs");
const axios = require("axios");

const argv = yargs
  .options({
    a: {
      demand: true,
      alias: "address",
      describe: "Address to fetch weather for",
      string: true
    }
  })
  .help()
  .alias("help", "h").argv;

const key = "NR6Yzom9rSApsMLtMZ1VYFlnGaCOICeM";
const domain = "http://open.mapquestapi.com/geocoding/v1";

var encodedAddress = encodeURIComponent(argv.address);
var geocodeUrl = `${domain}/address?key=${key}&location=${encodedAddress}`;

axios
  .get(geocodeUrl)
  .then(response => {
    if (response.data.info.statuscode === 400) {
      throw new Error("Illegal argument from request");
    } else if (response.data.info.statuscode === 403) {
      throw new Error("API Key is wrong");
    } else if (response.data.info.statuscode === 500) {
      throw new Error("Error processing request");
    } else if (response.data.info.statuscode === 0) {
      var lat = response.data.results[0].locations[0].latLng.lat;
      var lng = response.data.results[0].locations[0].latLng.lng;
      var key = "7be8d242628e88a181da27110e7ea8f0";
      var weatherUrl = `https://api.darksky.net/forecast/${key}/${lat},${lng}`;
      axios.get(weatherUrl).then(response => {
        console.log(
          `It's currently ${
            response.data.currently.temperature
          }. It feels like ${response.data.currently.apparentTemperature}.`
        );
      });
    } else {
      console.log("Unknown error");
    }
  })
  .catch(e => {
    if (e.code === "ENOTFOUND") {
      console.log("Unable to connect to API servers");
    }
  });
