const request = require("request");
const key = "NR6Yzom9rSApsMLtMZ1VYFlnGaCOICeM";

/**
 * Retrieve the latitude and longitude address from user input
 *
 * @param {*} address
 */
var geocodeAddress = (address, callback) => {
  request(
    {
      url: `http://open.mapquestapi.com/geocoding/v1/address?key=${key}&location=${encodeURIComponent(
        address
      )}`,
      json: true
    },
    (error, response, body) => {
      if (error) {
        callback("Unable to connect to mapquest.com server");
      } else if (body.info.statuscode === 400) {
        callback("Illegal argument from request");
      } else if (body.info.statuscode === 403) {
        callback("API Key is wrong");
      } else if (body.info.statuscode === 500) {
        callback("Error processing request:");
      } else if (body.info.statuscode === 0) {
        callback(undefined, {
          address: body.results[0].providedLocation.location,
          latitude: body.results[0].locations[0].latLng.lat,
          longitude: body.results[0].locations[0].latLng.lng
        });
      } else {
        callback("Unknown error");
      }
    }
  );
};

module.exports.geocodeAddress = geocodeAddress;
