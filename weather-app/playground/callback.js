/**
 * Get user from server
 *
 * @param {*} id of user
 * @param {*} callback of handling function
 */
var getUser = (id, callback) => {
  var user = {
    id: id,
    name: "Vikram"
  };

  // using setTimeout to simulate
  // real getUser function from server
  setTimeout(() => callback(user), 3000);
};

// call the function with callback
getUser(31, user => {
  console.log(user);
});
