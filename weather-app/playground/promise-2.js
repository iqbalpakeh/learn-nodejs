const request = require("request");
const domain = "http://open.mapquestapi.com/geocoding/v1";
const key = "NR6Yzom9rSApsMLtMZ1VYFlnGaCOICeM";

/**
 * Retrieve the latitude and longitude address from user input
 *
 * @param {*} address
 */
var geocodeAddress = address => {
  return new Promise((resolve, reject) => {
    request(
      {
        url: `${domain}/address?key=${key}&location=${encodeURIComponent(
          address
        )}`,
        json: true
      },
      (error, response, body) => {
        if (error) {
          reject("Unable to connect to mapquest.com server");
        } else if (body.info.statuscode === 400) {
          reject("Illegal argument from request");
        } else if (body.info.statuscode === 403) {
          reject("API Key is wrong");
        } else if (body.info.statuscode === 500) {
          reject("Error processing request:");
        } else if (body.info.statuscode === 0) {
          resolve({
            address: body.results[0].providedLocation.location,
            latitude: body.results[0].locations[0].latLng.lat,
            longitude: body.results[0].locations[0].latLng.lng
          });
        } else {
          reject("Unknown error");
        }
      }
    );
  });
};

geocodeAddress("19146").then(
  location => {
    console.log(JSON.stringify(location, undefined, 2));
  },
  errorMessage => {
    console.log(errorMessage);
  }
);
