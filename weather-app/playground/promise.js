/**
 * Asynchronous Add Operation (using dummy setTimeout to simulate delay)
 *
 * @param {*} a
 * @param {*} b
 */
var asyncAdd = (a, b) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (typeof a === "number" && typeof b === "number") {
        resolve(a + b);
      } else {
        reject("Arguments must be numbers");
      }
    }, 1500);
  });
};

asyncAdd(5, 7)
  .then(res => {
    console.log("Result #1: ", res);
    return asyncAdd(res, 33);
  })
  .then(res => {
    console.log("Result #2: ", res);
  })
  .catch(errorMessage => {
    console.log(errorMessage);
  });

// /**
//  * Sample usage of Promise
//  */
// var somePromise = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     // resolve("Hey. It worked!");
//     reject("Unable to fulfill promise");
//   }, 2500);
// });

// /**
//  * Main
//  */
// somePromise.then(
//   message => {
//     console.log("Success: ", message);
//   },
//   errorMessage => {
//     console.log("Error: ", errorMessage);
//   }
// );
