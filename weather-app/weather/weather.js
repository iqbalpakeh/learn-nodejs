const request = require("request");
const key = "7be8d242628e88a181da27110e7ea8f0";

/**
 * Retrieve the current temprature and apparent temprature from
 * the lat and lng location
 *
 * @param {*} lat
 * @param {*} lng
 * @param {*} callback
 */
var getWeather = (lat, lng, callback) => {
  request(
    {
      url: `https://api.darksky.net/forecast/${key}/${lat},${lng}`,
      json: true
    },
    (error, response, body) => {
      if (error) {
        callback("Unable to connect to mapquest.com server");
      } else if (body.code === 400) {
        callback(body.error);
      } else {
        callback(undefined, {
          temperature: body.currently.temperature,
          apparentTemperature: body.currently.apparentTemperature
        });
      }
    }
  );
};

module.exports.getWeather = getWeather;
